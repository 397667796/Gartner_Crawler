from unittest import TestCase
from parseSearchResult import parseSearchResult
from parseSearchResult import parseItemDetail
from parseSearchResult import parseItemDetailViaUrl


class TestParseSearchResult(TestCase):
    def test_parseSearchResult(self):
        data = open("./testdata/search_result_demo.html")
        parseSearchResult(data)


class TestParseItemDetail(TestCase):
    def test_parseItemDetail(self):
        data = open("./testdata/item_detail_page.html")
        parseItemDetail(data)


class TestParseItemDetailViaUrl(TestCase):
    def test_parseItemDetailViaUrl(self):
        parseItemDetailViaUrl(
            "https://www.gartner.com/doc/3411718?ref=SiteSearch&sthkw=cloud%20computing&fnl=search&srcId=1-3478922254")


