# Gartner 爬虫
* 因Gartner搜索页面使用下拉加载更多，同时暂时未分析如何抓取更多（使用Python抓取时记录模仿浏览器，或者直接在浏览器中使用js加框）
* 搜索结果页面示例：https://www.gartner.com/search/site/premiumresearch/binSearch?binValue=date-secs%3D%3DSeptember%202017&qid=
* 通过搜结果页面进入详情页。
* 爬取后，使用Google自动翻译，但不是官方API，翻译比较慢，每条近10秒钟。
